#/bin/bash

rm -rf ~/.vimrc > /dev/null 2>&1
rm -rf ~/.vim > /dev/null 2>&1

ln -s ~/dotvim/.vimrc ~/.vimrc
ln -s ~/dotvim/.vim ~/.vim

cd ~/.vim/pack/git-plugins/start/YouCompleteMe
python3 install.py --clangd-completer


